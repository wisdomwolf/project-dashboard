import os
import sys
import types
from datetime import datetime
# from sharepoint_access import SharepointFSS
import us
import time
from pytz import timezone
import subprocess
import ast
import pickle
# from pyremedy_fss import pyremedy_fss

DATE_FORMAT = '%a %m/%d/%Y %H:%M %Z'
SECTOR_LIST = [
    'Bon Appetit',
    'Canteen',
    'Chartwell',
    'Culinart',
    'Eurest',
    'Flik',
    'Morrison',
    'RA',
    'Touchpoint',
    'Wolfgang Puck'
]

BSM_LIST = [
    'TBD',
    'None'
]

TPA_LIST = [
    'TBD',
    'None'
]


def show_last_modified(unit):
    if unit.get('last_modified'):
        return unit['last_modified'].strftime(DATE_FORMAT)
    else:
        return 'N/A'


def get_created_date(unit):
    return unit['_id'].generation_time.strftime(DATE_FORMAT)


def inner_get(unit, key, inner_key):
    """
    Retrieves keys from inner dictionaries or returns sane value if outer dictionary doesn't exist
    @param unit:
    @param key: outer dictionary to retrieve @inner_key from
    @param inner_key:
    @return: value of unit[@key][@inner_key] if it exists
    """
    if unit.get(key):
        return unit.get(key).get(inner_key)
    else:
        print('Unable to retrieve {}'.format(inner_key))
        return None


def generate_datetime_string(dt, tz=None):
    if dt:
        if not tz:
            dt = (dt.replace(tzinfo=timezone('UTC'))).astimezone(timezone('US/Eastern'))
        return '{dt.month}/{dt.day}/{dt:%y} {dt.hour}:{dt:%M}'.format(dt=dt)
    else:
        return ''


def create_datetime_object(data, key):
    date_string = data[key]
    date_obj = datetime.strptime(date_string, '%Y-%m-%d')
    return date_obj


def generate_date_value(date):
    if type(date) is str:
        try:
            date = datetime.strptime(date, '%m/%d/%Y')
        except ValueError:
            pass
    if type(date) is datetime:
        date = date.strftime('%Y-%m-%d')

    return date


def generate_date_string(date):
    return date.strftime('%m/%d/%Y')


def create_cc_object(data):
    device = {}
    try:
        device['qty'] = int(data['cc_qty'])
        device['type'] = data['cc_type']
        return device
    except ValueError:
        device['qty'] = 0
        device['type'] = 'None'
        return device
    except KeyError:
        return False


def create_device_objects(data, prefix, max_rows=None):
    devices = []
    for i in range(1, max_rows + 1):
        device = {}
        try:
            device['qty'] = data['{}_qty{}'.format(prefix, i)]
            if '' == device['qty']:
                break
            else:
                device['qty'] = int(device['qty'])
            device['location'] = data['{}_location{}'.format(prefix, i)]
            device['type'] = data['{}_type{}'.format(prefix, i)]
            devices.append(device)
        except KeyError:
            return False
    return devices


def create_contact_object(data, prefix, outline):
    contact = {}
    for key, form_key in outline.items():
        try:
            if data.get('{}_{}'.format(prefix, form_key), '') == '':
                continue
            contact[key] = data['{}_{}'.format(prefix, form_key)]
        except KeyError:
            return False
    return contact


def create_whole_object(data_source, outline, required_items=None):
    required_items = required_items or []
    new_object = {}
    for key, value in outline.items():
        if not isinstance(value, dict) and not isinstance(value, types.LambdaType):
            if value not in required_items and '' == data_source.get(value, ''):
                continue
            new_object[key] = data_source.get(value)
            if not new_object[key]:
                print('missing value for {}'.format(key))
        elif type(value) is dict:
            new_object[key] = {}
            for k, v in value.items():
                if v(data_source):
                    new_object[key][k] = v(data_source)
                else:
                    continue
        else:
            if value(data_source):
                new_object[key] = value(data_source)
            else:
                continue
    return new_object


def create_sharepoint_connection():
    server_url = "https://sharepoint.example.com/"
    domain = 'DOMAIN\\'
    username = os.environ.get('SP_USER')
    pos_data_id = 'dd0a412a-80a2-424b-bb1e-212f3c5fa495'
    pw = os.environ.get('SP_PASS')

    sp = SharepointFSS(server_url, domain, username, pos_data_id, pw)

    return sp


def get_state_list():
    return sorted(list(us.states.mapping('abbr', 'name', us.states.STATES_AND_TERRITORIES).keys()))


def get_initials(full_name):
    name = full_name.split()
    result = '{}{}'.format(name[0][0], name[1][0])
    return result


def get_workstation_count(db, unit_number):
    cursor = db.unit_info.aggregate([
        {'$match': {"unit_number": unit_number}},
        {'$group': {
            '_id': 0,
            'sum': {'$sum': '$network_devices.pos_terms.qty'}
        }}
    ])
    return cursor.next().get('sum')


def add_network_devices_to_dict(data_dict, unit_obj, device_type):
    prefix = device_type.split('_')[0]
    if inner_get(unit_obj, 'network_devices', device_type):
        for index, device in enumerate(inner_get(
                unit_obj, 'network_devices', device_type), start=1):
            for key, value in device.items():
                data_dict['{}_{}{}'.format(prefix, key, index)] = value

    return data_dict


def get_network_devices(data_dict, unit_obj):
    network_device_types = ['pos_terms', 'others']
    for device_type in network_device_types:
        data_dict = add_network_devices_to_dict(data_dict, unit_obj, device_type)
    return data_dict


def get_log_text(unit_number, db):
    log = db.unit_info.find_one({'unit_number': unit_number})['log']
    entries = []
    for entry in log:
        for date, details in entry.items():
            entries.append('{} - {}'.format(date, details))
    return '\n'.join(entries)


def list_unit_emails(unit_number):
    results = []
    email_path = ''
    unit_dir = os.path.join('/var/www/email', unit_number)
    if os.path.isdir(unit_dir):
        for file in os.listdir(unit_dir):
            email_data = {}
            if os.path.isdir(file):
                for f in os.listdir(file):
                    email_file = os.path.join(unit_dir, file, f)
                    email_path = os.path.join(unit_number, file, f)
                    email_title = f
            else:
                email_file = os.path.join(unit_dir, file)
                email_path = os.path.join(unit_number, file)
                email_title = file
            email_data['content'] = email_path
            email_data['file_time'] = datetime.fromtimestamp(os.path.getmtime(email_file))
            email_data['time'] = '{dt.month}/{dt.day}/{dt:%y} {dt.hour}:{dt:%M}'.format(dt=email_data['file_time'])
            email_data['title'] = email_title
            results.append(email_data)
    results.sort(key=lambda x: x['file_time'])
    return results


def compare_golive_dates(unit_date, remedy_dates):
    try:
        dates = list(remedy_dates.values())
    except AttributeError:
        print("ERROR:", sys.exc_info()[0], sys.exc_info()[1])
        return {'error': True}
    unit_date = unit_date.replace(tzinfo=None)
    if unit_date in dates:
        dates.remove(unit_date)
    return {x: y for x, y in remedy_dates.items() if y in dates}


def get_remedy_dates(unit_number):
    if not isinstance(unit_number, str):
        unit_number = "{}".format(unit_number)
    remedy_dates = pyremedy_fss.get_golive_dates(unit_number)
    if remedy_dates:
        return {x: datetime.strptime(y, '%m-%d-%Y') for x, y in remedy_dates.items()}
    else:
        print("ERROR: pyremedy_fss did not return date information for unit {}".format(unit_number))
        return None
