__author__ = 'WisdomWolf'

import os
import project_dashboard
import unittest
import pdb
from selenium import webdriver

class ProjectTestCase(unittest.TestCase):

    def setUp(self):
        project_dashboard.app.config['MONGO_URI'] = 'mongodb://database:27017/test_db'
        project_dashboard.app.config['TESTING'] = True
        self.app = project_dashboard.app.test_client()
        self.driver = webdriver.Firefox()

    def tearDown(self):
        self.driver.quit()

    def login(self, username, password):
        return self.app.post('/login', data=dict(
            username=username,
            password=password
        ), follow_redirects=True)

    def logout(self):
        return self.app.get('/logout', follow_redirects=True)

    def test_home_status_code(self):
        # sends HTTP GET request to the application
        # on the specified path
        result = self.app.get('/')

        # assert the status code of the response
        self.assertEqual(result.status_code, 200)

    def test_login_logout(self):
        rv = self.login('admin', 'admin')
        assert b'You were logged in' in rv.data
        rv = self.logout()
        assert b'You were logged out' in rv.data
        rv = self.login('adminx', 'default')
        assert b'Invalid username' in rv.data
        rv = self.login('admin', 'default')
        assert b'Invalid password' in rv.data

    def test_adding_unit(self):
        self.login('admin', 'admin')
        rv = self.app.post('/add', data=dict(
            unit_number='12345',
            unit_name='Test Unit',
            sector='Eurest',
            landline='000-000-0000',
            assigned_bsm='Testy Sol',
            assigned_tpa='Ryan Beaman',
            golive_date='2099-12-31',
            master_account='Testing',
            svc_street_address='123 Main St',
            svc_city='Somewhere',
            svc_state='CT',
            svc_zip='12345',
            same_shipping_address='on',
            ship_street_address='123 Main St',
            ship_city='Somewhere',
            ship_state='CT',
            ship_zip='12345',
            contact_name='Unit Manager',
            contact_phone='704-555-1234',
            contact_phone2='704-555-4321',
            contact_email='unit.manager@compass-usa.com',
            client_it_name='Client IT',
            client_it_phone='301-555-1234',
            client_it_phone2='301-555-4321',
            client_it_email='clientit@client.com',
            pos_qty1=1,
            pos_type1='infogenesis',
            pos_location1='Cafe',
            pos_qty2=1,
            pos_type2='nextep',
            pos_location2='Cafe',
            pos_qty3='',
            pos_type3='',
            pos_location3='',
            pos_qty4='',
            pos_type4='',
            pos_location4='',
            pos_qty5='',
            pos_type5='',
            pos_location5='',
            other_qty1='',
            other_type1='',
            other_location1='',
            other_qty2='',
            other_type2='',
            other_location2='',
            other_qty3='',
            other_type3='',
            other_location3='',
            other_qty4='',
            other_type4='',
            other_location4='',
            other_qty5='',
            other_type5='',
            other_location5='',
            other_qty6='',
            other_type6='',
            other_location6='',
            other_qty7='',
            other_type7='',
            other_location7='',
            other_qty8='',
            other_type8='',
            other_location8='',
            other_qty9='',
            other_type9='',
            other_location9='',
            cc_qty='',
            cc_type='',
            comments=''
        ))

        assert b'successfully added' in rv.data

    def test_duplicate_addition(self):
        self.login('admin', 'admin')
        rv = self.app.post('/add', data=dict(
            unit_number='1234',
            unit_name='Test Unit',
            sector='Eurest',
            landline='000-000-0000',
            assigned_bsm='Testy Sol',
            assigned_tpa='Ryan Beaman',
            golive_date='2099-12-31',
            master_account='Testing',
            svc_street_address='123 Main St',
            svc_city='Somewhere',
            svc_state='CT',
            svc_zip='12345',
            same_shipping_address='on',
            ship_street_address='123 Main St',
            ship_city='Somewhere',
            ship_state='CT',
            ship_zip='12345',
            contact_name='Unit Manager',
            contact_phone='704-555-1234',
            contact_phone2='704-555-4321',
            contact_email='unit.manager@compass-usa.com',
            client_it_name='Client IT',
            client_it_phone='301-555-1234',
            client_it_phone2='301-555-4321',
            client_it_email='clientit@client.com',
            pos_qty1=1,
            pos_type1='infogenesis',
            pos_location1='Cafe',
            pos_qty2=1,
            pos_type2='nextep',
            pos_location2='Cafe',
            pos_qty3='',
            pos_type3='',
            pos_location3='',
            pos_qty4='',
            pos_type4='',
            pos_location4='',
            pos_qty5='',
            pos_type5='',
            pos_location5='',
            other_qty1=1,
            other_type1='Compass PC',
            other_location1='Office',
            other_qty2=1,
            other_type2='Order Status Monitor',
            other_location2='Cafe',
            other_qty3='',
            other_type3='',
            other_location3='',
            other_qty4='',
            other_type4='',
            other_location4='',
            other_qty5='',
            other_type5='',
            other_location5='',
            other_qty6='',
            other_type6='',
            other_location6='',
            other_qty7='',
            other_type7='',
            other_location7='',
            other_qty8='',
            other_type8='',
            other_location8='',
            other_qty9='',
            other_type9='',
            other_location9='',
            cc_qty=2,
            cc_type='Ingenico iPP350',
            comments='notes and things and stuff & moar stuff'
        ))

        assert b'already exists' in rv.data

    def test_unit_lookup(self):
        self.driver.get('/unit_lookup/12345')
        golive_date = self.driver.find_element_by_id('golive_date').get_attribute('value')
        print(golive_date)

    def test_delete_unit(self):
        result = self.app.post('/_delete', data=dict(
            unit_number='12345'
        ))
        print(result)



if __name__ == '__main__':
    unittest.main()
