#!/usr/bin/env python

# all the imports
import json
import os
import pdb
import sys
import time
import requests
from bson.json_util import dumps
from flask import Flask, request, session, redirect, url_for, \
    abort, render_template, flash, send_from_directory, jsonify, \
    Response, render_template_string
from flask_cors import CORS
from flask_jsglue import JSGlue
from pymongo import MongoClient, errors
from flask_mail import Mail
from flask_sqlalchemy import SQLAlchemy
from flask_user import login_required, UserManager, UserMixin, SQLAlchemyAdapter
from flask_user.forms import RegisterForm
from flask_debugtoolbar import DebugToolbarExtension
import logging

from wtforms import ValidationError, StringField
from wtforms.validators import InputRequired

from utils import *
from config import *
# import pdf_completer

LAST_RELOAD_TIME = datetime.now()
DOMAINS_ALLOWED = ['gmail.com']
app = Flask(__name__)
__version__ = 68

# configuration
FLASK_MODE = os.getenv('FLASK_MODE', 'DEVELOPMENT')
if FLASK_MODE == 'PRODUCTION':
    app.config.from_object('config.ProductionConfig')
elif FLASK_MODE == 'TESTING':
    app.config.from_object('config.TestingConfig')
elif FLASK_MODE == 'DEVELOPMENT':
    app.config.from_object('config.DevelopmentConfig')
else:
    logging.error('{} is not recgonized as a supported mode'.format(FLASK_MODE))
    sys.exit()

logging.critical('Flask started in {} mode'.format(app.config['CONFIG_TYPE']))
# Initialize Flask extensions
jsglue = JSGlue(app)
CORS(app)
db = SQLAlchemy(app)
mail = Mail(app)
#toolbar = DebugToolbarExtension(app)


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)

    # User authentication information
    username = db.Column(db.String(50), nullable=False, unique=True)
    password = db.Column(db.String(255), nullable=False, server_default='')
    reset_password_token = db.Column(db.String(100), nullable=False, server_default='')

    # User email information
    email = db.Column(db.String(255), nullable=False, unique=True)
    confirmed_at = db.Column(db.DateTime())

    # User information
    active = db.Column('is_active', db.Boolean(), nullable=False, server_default='0')
    first_name = db.Column(db.String(100), nullable=False, server_default='')
    last_name = db.Column(db.String(100), nullable=False, server_default='')
    role = db.Column(db.String(100), nullable=False, server_default='solution_manager')

    # Relationships
    roles = db.relationship('Role', secondary='user_roles',
            backref=db.backref('users', lazy='dynamic'))


class Role(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(50), unique=True)


class UserRoles(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    user_id = db.Column(db.Integer(), db.ForeignKey('user.id', ondelete='CASCADE'))
    role_id = db.Column(db.Integer(), db.ForeignKey('role.id', ondelete='CASCADE'))


class MyRegisterForm(RegisterForm):
    first_name = StringField('First name')
    last_name = StringField('Last name')
    email = StringField('Email', [InputRequired()])
    role = 'solution_manager'

    def validate_email(form, field):
        email_domain = field.data.split('@')[-1]
        if email_domain.lower() not in DOMAINS_ALLOWED:
            raise ValidationError("You're not allowed to register from this email provider.")


# Create all database tables
db.create_all()

# Setup Flask-User
db_adapter = SQLAlchemyAdapter(db, User)  # Register the User model
user_manager = UserManager(db_adapter, app, register_form=MyRegisterForm)  # Initialize Flask-User

client = MongoClient(app.config['MONGODB_URI'], connect=False, tz_aware=True)
mongo_db = client.get_database()

STATUS_VALUE = {'NOT_STARTED': 0, 'ORDERED': 1, 'GATHERING_REQ': 2,
                'INSTALLED': 2, 'SHIPPING_CONFIRMED': 2,
                'COMPLETED': 3, 'N/A': 3}

FP_DEVICE_ORDER_SUFFIX = app.config['FP_DEVICE_ORDER_SUFFIX']
TEMP_FOLDER = app.config['TEMP_FOLDER']

EASTERN = app.config['EASTERN']

CONTACT_OBJECT_MAP = {'name': 'name',
                      'phone': 'phone',
                      'phone2': 'phone2',
                      'email': 'email'}

ADDRESS_OBJECT_MAP = {'street': 'street_address',
                      'street_2': 'street_address2',
                      'city': 'city',
                      'state': 'state',
                      'zip': 'zip'}

OBJECT_FORM_MAP = dict(
    unit_number='unit_number', unit_name='unit_name', sector='sector',
    project_analyst='assigned_tpa', solution_manager='assigned_bsm',
    master_account='master_account',
    unit_contact=lambda x: create_contact_object(
        x, 'contact', CONTACT_OBJECT_MAP
    ),
    client_it_contact=lambda x: create_contact_object(
        x, 'client_it', CONTACT_OBJECT_MAP
    ),
    cc_devices=lambda x: create_cc_object(x),
    network_devices=dict(
        pos_terms=lambda x: create_device_objects(
            x, 'pos', app.config.get('POS_ROWS')
        ),
        others=lambda x: create_device_objects(
            x, 'other', app.config.get('OTHER_DEVICE_ROWS')
        )
    ),
    service_address=lambda x: create_contact_object(
        x, 'svc', ADDRESS_OBJECT_MAP
    ),
    shipping_address=lambda x: create_contact_object(
        x, 'ship', ADDRESS_OBJECT_MAP
    ),
    landline='landline',
    golive_date=lambda x: create_datetime_object(
        x, 'golive_date'
    ),
    notes='comments',
    same_shipping_address='same_shipping_address',
    payment_task='payment_task', network_task='network_task',
    pos_task='pos_task', treasury_order1='treasury_order1',
    treasury_order2='treasury_order2', compassnet_order='compassnet_order',
    bb_order_status='bb_order_status',
    router_install_status='router_install_status',
    network_diagram_status='network_diagram_status',
    router_order_status='router_order_status',
    merchantlink_status='merchantlink_status',
    cc_boarding_status='cc_boarding_status',
    cc_order_status='cc_order_status',
    mid_request_status='mid_request_status',
    project_status='project_status',
    service_provider='service_provider',
    net_account_number='net_account_number',
    first_ip='first_ip',
    last_ip='last_ip',
    gateway_ip='gateway_ip',
    dns_1='dns_1',
    dns_2='dns_2',
    subnet_mask='subnet_mask',
    download_speed='download_speed',
    upload_speed='upload_speed',
    network_comments='network_comments'
)

SHAREPOINT_DATA_MAP = dict(
    bb_order_status='Order_x0020_Broadband',
    router_install_status='Router_x0020_Installation',
    network_diagram_status='Network_x0020_Diagram',
    router_order_status='Order_x0020_Router_x002f_Switch',
    merchant_link_status='Merchant_x0020_Link_x0020_Setup',
    cc_boarding_status='FP_x0020_Boarding',
    cc_order_status='P2PE_x0020_Device_x0020_Status',
    mid_request_status='MID',
    project_status='Project_x0020_Status'
)

REQUIRED_ITEMS = ['unit_number', 'unit_name', 'sector', 'project_analyst', 'solution_manager']


@app.context_processor
def inject_vars():
    return dict(favicon=url_for('static', filename=app.config['FAVICON_URI']),
                version=__version__,
                solution_managers=BSM_LIST,
                project_analysts=TPA_LIST)


def create_sharepoint_connection():
    server_url = "https://example.com/"
    domain = 'DOMAIN\\'
    username = os.environ.get('SP_USER')
    pos_data_id = 'dd0a412a-80a2-424b-bb1e-212f3c5fa495'
    pw = os.environ.get('SP_PASS')

    # connection = SharepointFSS(server_url, domain, username, pos_data_id, pw)

    # return connection
    return False


SP = create_sharepoint_connection()


@login_required
@app.route('/_refresh_sp_instance')
def refresh_sp_instance():
    global SP
    SP = create_sharepoint_connection()
    print('Sharepoint instance refreshed')
    flash('Sharepoint instance refreshed')
    return redirect(url_for('unit_info'))


@app.route('/')
@login_required
def unit_info():
    return render_template('unit_info.html',
                           other_range=range(1, app.config['OTHER_DEVICE_ROWS'] + 1),
                           pos_range=range(1, app.config['POS_ROWS'] + 1),
                           sectors=SECTOR_LIST,
                           states=get_state_list())


@app.route('/unit_lookup/')
@login_required
def show_unit_lookup(unit_number=None):
    unit = None
    if unit_number:
        unit = mongo_db.unit_info.find_one({
            'unit_number': unit_number
        })

    return render_template('unit_lookup.html',
                           unit=unit,
                           other_range=range(1, app.config['OTHER_DEVICE_ROWS'] + 1),
                           pos_range=range(1, app.config['POS_ROWS'] + 1),
                           sectors=SECTOR_LIST,
                           states=get_state_list())


@app.route('/userscripts/')
@login_required
def show_userscript_links():
    return render_template('userscript_links.html', last_reload_time=LAST_RELOAD_TIME)


# noinspection PyShadowingBuiltins
@app.route('/testing')  # you can probably even put testing.py here
def testing():
    vars = request.args
    print('secret key: {}'.format(app.secret_key))
    return ','.join(map(str, vars.items()))


@app.route('/_update_sp', methods=['POST'])
def update_sp(unit_number=None, field_name=None, status=None):
    response = None
    is_ajax_request = False
    if not unit_number:
        is_ajax_request = True
    unit_number = unit_number or request.form.get('unit_number')
    field = field_name or request.form.get('field_name')
    value = status or request.form.get('status')
    unit = SP.lookup_unit(unit_number)
    do_update_log = request.form.get('update_log')
    # db.unit_info.update_one({'unit_number': '32179'}, {'$currentDate': {'cc_order_status_timestamp': True},
    #                                                    '$set': {'test_value2': 'testing 321...'}})
    if is_ajax_request and do_update_log:
        log_field = request.form.get('log_name')
        user_name = request.form.get('user_name')
        mongo_db.unit_info.update_one(
            {'unit_number': unit_number},
            {
                '$push':
                    {'log':
                         {'{dt.month}/{dt.day}/{dt:%y} {dt.hour}:{dt:%M}'.format(dt=datetime.now()):
                              '{} changed {} to {}'.format(user_name, log_field, value)
                          }
                     },
                '$currentDate':
                    {'{}_timestamp'.format(log_field): True}
            }
        )
    response = SP.set_field(unit, field, value)
    return jsonify(response=response)


@app.route('/sp_unit_lookup')
def sp_unit_lookup(unit_number=None):
    unit_number = unit_number or request.args.get('unit_number')
    print('looking up unit {} in sharepoint'.format(unit_number))
    unit = SP.lookup_unit(unit_number)
    if unit:
        return jsonify(SP.fetch_fields(unit))
    else:
        return jsonify(unit_number=0, errors=True)


# TODO: abolish this function
@app.route('/_unit_exists')
def unit_exists():
    unit_number = request.args.get('unit_number')
    unit = mongo_db.unit_info.find_one({'unit_number': unit_number})
    if unit:
        return jsonify(result=unit.get('unit_name'))
    else:
        return jsonify(result='')


@app.route('/_lookup_unit/')
def lookup_unit():
    unit_number = request.args.get('unit_number')
    unit = mongo_db.unit_info.find_one({'unit_number': str(unit_number)}, {'_id': 0})
    if not unit:
        unit = {'unit_number': 0}

    return jsonify(unit)


@app.route('/_unit_lookup')
def unit_lookup():
    lookup_unit = request.args.get('lookup_unit')
    if lookup_unit.isdigit():
        unit = mongo_db.unit_info.find_one({'unit_number': str(lookup_unit)})
    else:
        return jsonify(redirect_url=url_for('show_overview_dashboard',
                                            search_text=lookup_unit,
                                            historic=1))

    if unit:
        unit_data = dict(
            unit_number=unit['unit_number'],
            unit_name=unit['unit_name'],
            created_date=get_created_date(unit),
            modified_date=show_last_modified(unit),
            sector=unit.get('sector', unit.get('unit_sector', 'Canteen')),
            landline=unit.get('landline'),
            solution_manager=unit.get('solution_manager'),
            project_analyst=unit.get('project_analyst'),
            golive_date=generate_date_value(unit.get('golive_date', '01-01-1970')),
            master_account=unit.get('master_account'),
            client_it_name=inner_get(unit, 'client_it_contact', 'name'),
            client_it_phone=inner_get(unit, 'client_it_contact', 'phone'),
            client_it_phone2=inner_get(unit, 'client_it_contact', 'phone2'),
            client_it_email=inner_get(unit, 'client_it_contact', 'email'),
            contact_name=inner_get(unit, 'unit_contact', 'name'),
            contact_phone=inner_get(unit, 'unit_contact', 'phone'),
            contact_phone2=inner_get(unit, 'unit_contact', 'phone2'),
            contact_email=inner_get(unit, 'unit_contact', 'email'),
            svc_street_address=inner_get(unit, 'service_address', 'street'),
            svc_city=inner_get(unit, 'service_address', 'city'),
            svc_state=inner_get(unit, 'service_address', 'state'),
            svc_zip=inner_get(unit, 'service_address', 'zip'),
            ship_street_address=inner_get(unit, 'shipping_address', 'street'),
            ship_city=inner_get(unit, 'shipping_address', 'city'),
            ship_state=inner_get(unit, 'shipping_address', 'state'),
            ship_zip=inner_get(unit, 'shipping_address', 'zip'),
            same_shipping_address=unit.get('same_shipping_address'),
            payment_task=unit.get('payment_task'),
            network_task=unit.get('network_task'),
            treasury_order1=unit.get('treasury_order1'),
            treasury_order2=unit.get('treasury_order2'),
            compassnet_order=unit.get('compassnet_order'),
            service_provider=unit.get('service_provider'),
            net_account_number=unit.get('net_account_number'),
            first_ip=unit.get('first_ip'),
            last_ip=unit.get('last_ip'),
            gateway_ip=unit.get('gateway_ip'),
            dns_1=unit.get('dns_1'),
            dns_2=unit.get('dns_2'),
            subnet_mask=unit.get('subnet_mask'),
            download_speed=unit.get('download_speed'),
            upload_speed=unit.get('upload_speed'),
            network_comments=unit.get('network_comments'),
            pos_task=unit.get('pos_task'),
            comments=unit.get('notes', ''),
            cc_qty=inner_get(unit, 'cc_devices', 'qty'),
            cc_type=inner_get(unit, 'cc_devices', 'type'),
            bb_order_status=unit.get('bb_order_status'),
            bb_order_status_timestamp=generate_datetime_string(unit.get('bb_order_status_timestamp')),
            router_install_status=unit.get('router_install_status'),
            router_install_status_timestamp=generate_datetime_string(unit.get('router_install_status_timestamp')),
            network_diagram_status=unit.get('network_diagram_status'),
            network_diagram_status_timestamp=generate_datetime_string(unit.get('network_diagram_status_timestamp')),
            router_order_status=unit.get('router_order_status'),
            router_order_status_timestamp=generate_datetime_string(unit.get('router_order_status_timestamp')),
            merchant_link_status=unit.get('merchant_link_status'),
            merchant_link_status_timestamp=generate_datetime_string(unit.get('merchant_link_status_timestamp')),
            cc_boarding_status=unit.get('cc_boarding_status'),
            cc_boarding_status_timestamp=generate_datetime_string(unit.get('cc_boarding_status_timestamp')),
            cc_order_status=unit.get('cc_order_status'),
            cc_order_status_timestamp=generate_datetime_string(unit.get('cc_order_status_timestamp')),
            mid_request_status=unit.get('mid_request_status'),
            mid_request_status_timestamp=generate_datetime_string(unit.get('mid_request_status_timestamp')),
            net_devices=dumps(unit.get('network_devices'), indent=2),
            project_status=unit.get('project_status')
        )
        unit_data = get_network_devices(unit_data, unit)

        return jsonify(unit_data)
    else:
        return jsonify([dict(type='danger', message='Unit not found.')])


@app.route('/all_current_units.json')
def all_current_units_json():
    return get_unit_list()


@app.route('/all_units.json')
def all_units_json():
    return get_unit_list(True)


def get_unit_list(include_historic=False):
    if not include_historic:
        match = {'golive_date': {'$gte': datetime.now()}}
    else:
        match = {}
    cursor = mongo_db.unit_info.aggregate([
        {'$project': {
            '_id': 0, 'unit_name': 1, 'unit_number': 1,
            'mid_request_status': 1, 'cc_order_status': 1,
            'cc_boarding_status': 1, 'bb_order_status': 1,
            'router_order_status': 1, 'network_diagram_status': 1,
            'merchantlink_status': 1, 'solution_manager': 1,
            'project_analyst': 1, 'golive_date': 1,
            'router_install_status': 1, 'project_status': 1
        }},
        {'$match':
            match
        }
    ])

    all_units = list(cursor)
    for unit in all_units:
        try:
            days_to_golive = (unit['golive_date'] - datetime.now(EASTERN)).days
        except TypeError:
            days_to_golive = (unit['golive_date'] - datetime.now()).days
        unit['golive_countdown'] = '{} Days'.format(days_to_golive)
        unit['days_to_golive'] = int(days_to_golive)

    return Response(dumps(all_units), mimetype='application/json')


@app.route('/get_json/<unit_number>')
def get_json(unit_number):
    data = mongo_db.unit_info.find_one(
        {'unit_number': unit_number}, {'_id': 0}
    )
    if data:
        return jsonify(data)
    else:
        err = 'Oops, unable to locate data for {}'.format(unit_number)
        return err


@app.route('/_add_to_sp', methods=['POST'])
def add_to_sharepoint():
    new_unit = request.form.to_dict()
    new_unit['api_key'] = app.secret_key
    result = requests.post('{}/_add_to_sp'.format(SP_ACCESS_URL), json=new_unit)
    print(result)
    try:
        return jsonify(result.json())
    except json.decoder.JSONDecodeError:
        print('Failed to add unit to SP')
        return jsonify(response='fail')


@app.route('/add', methods=['POST'])
@login_required
def add_new_unit(new_unit=None):
    new_unit = new_unit or create_whole_object(
        request.form, OBJECT_FORM_MAP, REQUIRED_ITEMS
    )
    try:
        result = mongo_db.unit_info.insert_one(new_unit)
    except errors.DuplicateKeyError:
        return jsonify([dict(type='danger', message='Oops, looks like this unit already exists')])
    # flash('New unit was successfully added.\n{}'.format(result.inserted_id))
    # return redirect(url_for('unit_info'))
    # try:
    #     result = 'New unit was successfully added.\n{}'.format(
    #         db.unit_info.insert_one(new_unit).inserted_id
    #     )
    # except errors.DuplicateKeyError:
    #     result = 'already exists'
    return jsonify([dict(type='success',
                         message='New unit was successfully added.\n{}'
                         .format(result.inserted_id))])


@app.route('/_delete', methods=['POST'])
@login_required
def delete_unit():
    unit_number = request.form.get('unit_number')
    result = mongo_db.unit_info.delete_one({'unit_number': unit_number})

    return jsonify(result=result.raw_result)


@app.route('/_update', methods=['POST'])
@login_required
def update_entry():
    unit_number = request.form.get('unit_number')
    new_unit = create_whole_object(
        request.form, OBJECT_FORM_MAP, REQUIRED_ITEMS
    )
    unit = mongo_db.unit_info.find_one({'unit_number': unit_number})
    if unit:
        try:
            if not new_unit.get('network_devices'):
                _ = new_unit.pop('network_devices')
            if not new_unit.get('cc_devices'):
                _ = new_unit.pop('cc_devices')
        except KeyError:
            pass
        mongo_db.unit_info.update({'_id': unit['_id']}, {'$currentDate': {'last_modified': True}, "$set": new_unit},
                                  upsert=False)
        result = update_sp(unit['unit_number'], 'm3p5', new_unit['golive_date'])
        print(result)
        return jsonify([dict(type='success', message='Successfully updated unit')])
    else:
        return jsonify([dict(type='danger', message='Something went wrong when attempting to update unit.')])


@login_required
@app.route('/download_order_pdf/<unit_number>')
def download_order_pdf(unit_number):
    unit = mongo_db.unit_info.find_one({'unit_number': unit_number})
    if unit:
        return send_from_directory(TEMP_FOLDER,
                                   '{}_{}_{}'.format(unit['unit_number'],
                                                     unit['unit_name'],
                                                     pdf_completer.DEVICE_ORDER_SUFFIX),
                                   as_attachment=True)
    else:
        return 'Nothing to see here.'


@app.route('/generate_order_pdf/<unit_number>')
def generate_order_pdf(unit_number):
    if pdf_completer.generate_order_form(unit_number, mongo_db):
        return redirect(url_for('download_order_pdf', unit_number=unit_number))
    else:
        return 'Failed to generate PDF.  I blame the goat.'


@login_required
@app.route('/download_switching_pdf/<unit_number>')
def download_switching_pdf(unit_number):
    unit = mongo_db.unit_info.find_one({'unit_number': unit_number})
    if unit:
        return send_from_directory(TEMP_FOLDER,
                                   '{}_{}_{}'.format(unit['unit_number'],
                                                     unit['unit_name'],
                                                     pdf_completer.FP_SWITCHING_SUFFIX),
                                   as_attachment=True)
    else:
        return 'Nothing to see here.'


@app.route('/generate_switching_pdf/<unit_number>')
def generate_switching_pdf(unit_number):
    if pdf_completer.generate_switching_form(unit_number, mongo_db):
        return redirect(url_for('download_switching_pdf', unit_number=unit_number))
    else:
        return 'Failed to generate PDF.  I blame the goat.'


@app.route('/overview/')
@app.route('/overview/<search_text>')
@login_required
def show_overview_dashboard(search_text=None):
    include_historic = request.args.get('historic')
    return render_template('overview.html', solution_managers=BSM_LIST,
                           search_text=search_text, historic_data=include_historic)


@app.route('/_get_email_links')
def get_email_links():
    unit_number = request.args.get('unit_number')
    return jsonify(result=list_unit_emails(unit_number))


@app.route('/_get_status_log')
def get_status_log():
    unit_number = request.args.get('unit_number')
    return jsonify(result=get_log_text(unit_number, mongo_db))


@app.route('/_get_golive_dates')
def get_golive_dates():
    unit_number = request.args.get('unit_number')
    unit_date = mongo_db.unit_info.find_one({'unit_number': unit_number}).get('golive_date')
    remedy_dates = get_remedy_dates(unit_number)
    mismatched_dates = compare_golive_dates(unit_date, remedy_dates)
    return jsonify(dates=mismatched_dates)


if __name__ == '__main__':
    port = int(os.environ.get("PORT", 5001))
    # app.run(host='0.0.0.0', port=port)
    app.run()
