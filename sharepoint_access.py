#!/usr/bin/env python
from urllib.error import HTTPError

from sharepoint import SharePointSite, basic_auth_opener
import code
from datetime import datetime
import pdb
import sharepoint
import sys
import os
import time

SERVER_URL = "https://sharepoint.example.com"
DOMAIN = 'DOMAIN\\'
USERNAME = os.environ.get('SP_USER')
POS_DATA_ID = 'dd0a412a-80a2-424b-bb1e-212f3c5fa495'
POS_DATA_ID2 = 'c4ffe2b7-6819-44f8-8faf-54ff2ee36ada'
PW = os.environ.get('SP_PASS')


class SharepointFSS(object):

    def __init__(self, server_url, domain, username, list_id, password):
        self.data = None
        self.server_url = server_url
        self.site_url = "{}fss".format(server_url)
        self.auth = "{}{}".format(domain, username)
        self.data_received_time = None
        self.list_id = list_id
        self.password = password
        self.site = self.connect_site()

    def connect_site(self):
        opener = basic_auth_opener(self.server_url, self.auth, self.password)
        self.data_received_time = datetime.now()
        return SharePointSite(self.site_url, opener)

    def fetch_list(self, second_attempt=False):
        print('fetching data...\n')
        if self.is_data_stale():
            self.site = self.connect_site()
            return self.fetch_list(True)
        try:
            return self.site.lists[self.list_id]
        except HTTPError:
            print('Unauthorized request, verify your password and retry.\n')
            print('HTTPError:', sys.exc_info()[1])
            if 'Unauthorized' in str(sys.exc_info()[1]) or second_attempt:
                print('closing connection')
                return None
            else:
                print('Authorized?')
            self.site = self.connect_site()
            return self.fetch_list(True)

    def lookup_unit(self, unit_number):
        result = None
        if not self.data or self.is_data_stale():
            self.data = self.fetch_list()
        for r in self.data.rows:
            if r.Unit_x0020__x0023_ == str(unit_number):
                print(r.Unit_x0020_Name_x0020_Billing, r.Unit_x0020__x0023_, r.id)
                result = r
                break
        else:
            print('Unable to locate {} in sharepoint'.format(unit_number))
        return result

    def print_fields(self, unit):
        if isinstance(unit, dict):
            for k,v in sorted(unit.items()):
                print('{0}: {1}'.format(k,v))
        elif isinstance(unit, sharepoint.lists.SharePointListRow):
            x = self.fetch_fields(unit)
            self.print_fields(x)
        else:
            print('invalid parameter')

    def fetch_fields(self, row):
        results = {}
        for field in dir(row):
            if field[0] != '_' and field[0].isupper():
                results[field] = getattr(row, field)
        return results

    def get_field(self, row, field_name):
        field_name = field_name.replace(' ', '_x0020_').replace('#', '_x0023_')
        field_name = field_name.replace('/', '_x002f_').replace('=', '_x003d_')
        if field_name == 'Custom_x0020_Item_x0020_Range':
            field_name = field_name + '_x0'
        return getattr(row, field_name)

    def set_field(self, row, field_name, value):
        field_name = field_name.replace(' ', '_x0020_').replace('#', '_x0023_')
        field_name = field_name.replace('/', '_x002f_').replace('=', '_x003d_')
        if field_name == 'Custom_x0020_Item_x0020_Range':
            field_name = field_name + '_x0'
        setattr(row, field_name, value)
        print('{}.{} set to {}'.format(row.Unit_x0020_Name_x0020_Billing, field_name, value))
        saved = self.save_changes()
        print('save status: {}'.format(saved))
        return saved

    def add_unit(self, unit):
        if not self.data:
            self.data = self.fetch_list()
        try:
            if any('ysys' in x for x in [x.get('type') for x in unit.get('network_devices').get('pos_terms')]):
                vendor = 'Agilysys'
                pos = 'IG'
            elif any('icros' in x for x in [x.get('type') for x in unit.get('network_devices').get('pos_terms')]):
                vendor = 'Micros'
                pos = 'SIMP'
            else:
                vendor = 'Other'
                pos = 'Other'
        except AttributeError:
            vendor = 'Other'
            pos = 'Other'

        if not isinstance(unit.get('golive_date'), datetime):
            golive_date = datetime.strptime(unit.get('golive_date'), '%Y-%m-%d')
        else:
            golive_date = unit.get('golive_date')

        project_analyst = unit.get('project_analyst').split()

        new_unit = {
            'Unit_x0020__x0023_': unit.get('unit_number'),
            'POS_': pos,
            'Vendor_': vendor,
            'Unit_x0020_Name_x0020_Common': unit.get('unit_name'),
            'Unit_x0020_Name_x0020_Billing': unit.get('unit_name'),
            'Sector': unit.get('sector'),
            'Go_x0020_Live': golive_date,
            'Assigned_x0020_BSM': unit.get('solution_manager'),
            'Project_x0020_Analyst': '{} {}.'.format(project_analyst[0], project_analyst[1][:1]),
            'Unit_x0020_Status': 'Pending'
        }
        self.data.append(new_unit)
        return self.save_changes()

    def save_changes(self):
        try:
            self.data.save()
            return True
        except AttributeError:
            print('Error 25')
            return True
        except:
            print(sys.exc_info())
            return False

    def is_data_stale(self):
        if (datetime.now() - self.data_received_time).seconds > 14400:
            return True
        else:
            return False


def print_field_names(row):
    for field in dir(row):
        if field[0] != '_' and field[0].isupper():
            print(field)


def get_default_instance():
    default = SharepointFSS(SERVER_URL, DOMAIN, USERNAME, POS_DATA_ID, PW)
    return default


def run_test(unit, unit_number, sp):
    start_time = time.time()
    print('Current Deployment Notes: {}'.format(unit.Deployment_x0020_Notes))
    unit = sp.lookup_unit(unit_number)
    print('Refreshed Deployment Notes: {}'.format(unit.Deployment_x0020_Notes))
    sp = get_default_instance()
    unit = sp.lookup_unit(unit_number)
    print('Reloaded Deployment Notes: {}'.format(unit.Deployment_x0020_Notes))
    end_time = time.time() - start_time
    print('run time: {}'.format(end_time))


def test_run(unit_number=None):
    sp = get_default_instance()
    unit_number = unit_number or '32179'
    unit = sp.lookup_unit(unit_number)
    print(sp.fetch_fields(unit))

if __name__ == '__main__':
    print('Welcome to Sharepoint for Pythons\n\n')
    #test_run()
    vars = globals().copy()
    vars.update(locals())
    shell = code.InteractiveConsole(vars)
    shell.interact()
